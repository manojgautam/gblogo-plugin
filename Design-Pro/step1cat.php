<script type='text/javascript' src='//code.jquery.com/jquery-2.2.3.min.js'></script>
<?php
global $wpdb;
    
if(isset($_POST['submit']))
{
	$table_name = $wpdb->prefix . "orderfrom_procategory";
	 $categoryname=$_POST['categoryname'];
	$rows= $wpdb->get_var("select COUNT(*) from $table_name where `name` ='$categoryname'");
	if($rows!=1){
	 $table_name = $wpdb->prefix . "orderfrom_procategory";
	$query = $wpdb->insert( $table_name, array(
		'name' => $categoryname,
		'short_code' => 'short_code_'.str_replace(' ','_',$categoryname)
		));

	$lastid = $wpdb->insert_id;
	$query = $wpdb->query("UPDATE $table_name SET `short_code`='pro_logo_shortcode id=$lastid' WHERE `cat_id`='$lastid'");
	if($query == 1) 
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('updated'); $('#setting-error-settings_updated').removeClass('error'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record saved.'); });</script>";
	}
	else
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('error'); $('#setting-error-settings_updated').removeClass('updated'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record has not been saved. Please try again!'); });</script>";

	}
	}else{
			echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('error'); $('#setting-error-settings_updated').removeClass('updated'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Name already exists. Please choose another'); });</script>";
	}
}
$table_name = $wpdb->prefix . "orderfrom_procategory";
	$rows = $wpdb->get_results("select * from $table_name ORDER BY `cat_id` DESC ");
?>
<script>
	$(document).ready(function(){
		$(".notice-dismiss").click(function(){
			$(this).parent.hide();
		});
	});
</script>
<div id="wpbody" role="main">

<div id="wpbody-content" aria-label="Main content" tabindex="0">
		
<div class="wrap">

<h1>Add New Logo & Stationery Category</h1>
<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
	<p>
		<strong></strong>
	</p>
	<button type="button" class="notice-dismiss">
		<span class="screen-reader-text">Dismiss this notice.</span>
	</button>
</div>

<form method="post" action="">

	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><label for="logoname">Category Name</label></th>
				<td>
					<input type="text" name="categoryname" value="" class="regular-text" required/>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
</form>
</div>
</div>
<h1>Category List</h1>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			 <tr>
				<th>S.No</th>
				<th>Category Name</th>
				<th>Short Code</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			 <?php 
			 $sno=1;
			 foreach($rows as $row){ ?>
			<tr><?php $id=$row->cat_id;?>
				<td><?php echo $sno;?></td>
				<td><?php echo $row->name;?></td>
				<td>[<?php echo $row->short_code; ?>]</td>
				<td><a href="admin.php?page=add-category&id=<?php echo $id ;?>">Delete</a></td>
			</tr>
			 <?php 
			 $sno++;
			 }?>
		</tbody>
	</table>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>

