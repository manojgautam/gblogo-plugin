<script type='text/javascript' src='//code.jquery.com/jquery-2.2.3.min.js'></script>
<?php
global $wpdb;
$id=$_GET['id'];
$did=$_GET['did'];
if($did != ""){
$table_name1 = $wpdb->prefix . "Orderfrom_pro";
$query = $wpdb->query("Delete from $table_name1 WHERE `logo_id`='$did'");
if($query == 1) 
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('updated'); $('#setting-error-settings_updated').removeClass('error'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record Deleted.'); });</script>";
	}
}
if(isset($_POST['submit']))
{

    $logoname=$_POST['logoname'];
	$prize=$_POST['price'];
	$category=$_POST['category'];
	$description=$_POST['description'];
	$publish=$_POST['publish'];
	$tax=$_POST['tax'];
	$checked=$_POST['showdescription'];
	if($checked==""){
	$checked="";
	}
	$table_name = $wpdb->prefix . "Orderfrom_pro";
	$query = $wpdb->insert( $table_name, array(
		'name' => $logoname,
		'price' =>$prize,
		'categoryname'=>$category,
		'description' => $description,
		'publish' => $publish,
		'showdescription' => $checked,
		'tax' => $tax
		));
	if($query == 1) 
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('updated'); $('#setting-error-settings_updated').removeClass('error'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record saved.'); });</script>";
	}
	else
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('error'); $('#setting-error-settings_updated').removeClass('updated'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record has not been saved. Please try again!'); });</script>";

	}
}
if(isset($_POST['update']))
{
	$upload_dir = wp_upload_dir();
	$logoname=$_POST['logoname'];
	$prize=$_POST['price'];
	$category=$_POST['category'];
	$description=$_POST['description'];
	$checked=$_POST['showdescription'];
	$publish=$_POST['publish'];
	$tax=$_POST['tax'];

	$table_name = $wpdb->prefix . "Orderfrom_pro";
	$query = $wpdb->query("UPDATE $table_name SET `name`='$logoname',`price`='$prize',`tax`='$tax',`description`='$description' ,`categoryname`='$category',`publish`='$publish',`showdescription`='$checked' WHERE `logo_id`='$id'");
	if($query == 1) 
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('updated'); $('#setting-error-settings_updated').removeClass('error'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Updated saved.'); });</script>";
	}
	else
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('error'); $('#setting-error-settings_updated').removeClass('updated'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record has not been saved. Please try again!'); });</script>";

	}
}
?>

<script>
	$(document).ready(function(){
		$(".notice-dismiss").click(function(){
			$(this).parent.hide();
		});
	});
</script>
<div id="wpbody" role="main">

<div id="wpbody-content" aria-label="Main content" tabindex="0">
<?php if($id=='' && $did ==''){?>		
<div class="wrap">
<h1>Add New Logo & Stationery Option</h1>

<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
	<p>
		<strong></strong>
	</p>
	<button type="button" class="notice-dismiss">
		<span class="screen-reader-text">Dismiss this notice.</span>
	</button>
</div>

<form method="post" action="" enctype="multipart/form-data">

	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><label for="logoname">Logo name</label></th>
				<td>
					<input type="text" name="logoname" value="" class="regular-text" required/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="price">Price</label></th>
				<td>
					<input type="text" name="price" value="" class="regular-text" required/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="tax">Tax</label></th>
				<td>
					<input type="text" name="tax" value="" class="regular-text" required/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="publish">Publish</label></th>
				<td>
					<select name="publish"><option>yes</option><option>No</option></select>
				</td>
			</tr>
			<?php 
			$table_name = $wpdb->prefix . "orderfrom_procategory";
	        $rows = $wpdb->get_results("select * from ".$table_name);
			?>
			<tr>
				<th scope="row"><label for="price">Category</label></th>
				<td>
					<select name="category" class="regular-text" required/>
					<?php foreach($rows as $name){?>
				  <option value="<?php echo $name->name;?>"><?php echo $name->name;?></option>
					<?php }?>
				</select>

				</td>
			</tr>
			<tr>
				<th scope="row"><label for="description">Description</label></th>
				<td>
					<textarea rows="4" cols="50"  name="description" class="regular-text"></textarea> 
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="description">Please uncheck if you do not want show Description</label></th>
				<td>
				<input type="checkbox" name="showdescription" value="checked" checked>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
</form>

</div><?php } else {?>
<div class="wrap">
<h1>Update Logo & Stationery Option</h1>

<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
	<p>
		<strong></strong>
	</p>
	<button type="button" class="notice-dismiss">
		<span class="screen-reader-text">Dismiss this notice.</span>
	</button>
</div>


<?php $table_name = $wpdb->prefix . "Orderfrom_pro";
	$result = $wpdb->get_results("select * from ".$table_name." where logo_id=".$id);
foreach($result as $row){?>
<form method="post" action="" enctype="multipart/form-data">
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><label for="logoname">Logo name</label></th>
				<td>
					<input type="text" name="logoname" value="<?php echo $row->name; ?>" class="regular-text" required/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="price">Price</label></th>
				<td>
					<input type="text" name="price" value="<?php echo $row->price; ?>" class="regular-text" required/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="tax">Tax</label></th>
				<td>
					<input type="text" name="tax" value="<?php echo $row->tax; ?>" class="regular-text" required/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="publish">Publish</label></th>
				<td>
					<select name="publish" value="<?php echo $row->publish; ?>"><option>yes</option><option>No</option></select>
				</td>
			</tr>
				<tr>
				<th scope="row"><label for="price">Category</label></th>
				<td>
					<input type="text" name="category" value="<?php echo $row->categoryname; ?>" class="regular-text" readonly/>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="description">Description</label></th>
				<td>
					<textarea rows="4" cols="50"  name="description" class="regular-text"><?php echo $row->description; ?></textarea> 
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="description">Please uncheck if you do not want show Description</label></th>
				<td>
				<input type="checkbox" name="showdescription" value="checked" <?php echo $row->showdescription; ?>>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="submit"><input type="submit" name="update" id="submit" class="button button-primary" value="Update Changes"></p>
</form>
<?php } ?>
</div>
<?php } ?>
<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>

<?php
	global $wpdb;
    $table_name = $wpdb->prefix . "Orderfrom_pro";
	$rows = $wpdb->get_results("select * from $table_name  ORDER BY `logo_id` DESC ");
?>
<div id="wpbody" role="main">

<div id="wpbody-content" aria-label="Main content" tabindex="0">
		
<div class="wrap">
<?php  echo "<h1>" . __( 'Logo Listing' ) . "</h1>"; ?>

<div id="setting-error-set   tings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
	<p>
		<strong></strong>
	</p>
	<button type="button" class="notice-dismiss">
		<span class="screen-reader-text">Dismiss this notice.</span>
	</button>
</div>
	<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			 <tr>
				<th>S.No</th>
				<th>Name</th>
				<th>Price</th>
				<th>Tax</th>
				<th>Publish</th>
				<th>Category</th>	
				<th>Description</th>
				<th>Action</th>
				
             </tr>
		</thead>
		<tbody>
			 <?php 
			 $sno=1;
			 foreach($rows as $row){ ?>
			<tr><?php $id=$row->logo_id;?>
				<td><?php echo $sno;?></td>
				<td><?php echo $row->name;?></td>
				<td><?php echo $row->price;?></td>
				<td><?php echo $row->tax;?></td>
				<td><?php echo $row->publish;?></td>
				<td><?php echo $row->categoryname;?></td>
				<td><?php echo $row->description;?></td>
				<td><a href="admin.php?page=add-prologo&id=<?php echo $id ;?>">Edit</a>|<a href="admin.php?page=add-prologo&did=<?php echo $id ;?>">Delete</a></td>
			</tr>
			 <?php 
			 $sno++;
			 }?>
		</tbody>
	</table>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>

