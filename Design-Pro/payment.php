<script type='text/javascript' src='//code.jquery.com/jquery-2.2.3.min.js'></script>
<?php
global $wpdb;

if(isset($_POST['submit']))
{
	$gateway=$_POST['gateway'];
	$payemail=$_POST['payemail'];
	 $table_name = $wpdb->prefix . "payemetn_pro";
	 $query = $wpdb->query("UPDATE $table_name SET `pro_gateway`='$gateway',`pro_payemail`='$payemail' WHERE `id`='1'");
	if($query == 1) 
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('updated'); $('#setting-error-settings_updated').removeClass('error'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Updated saved.'); });</script>";
	}
	else
	{
		echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('error'); $('#setting-error-settings_updated').removeClass('updated'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Record has not been saved. Please try again!'); });</script>";

	}
}

	$table_name = $wpdb->prefix . "payemetn_pro";
	         $row = $wpdb->get_results("select * from $table_name  ORDER BY `id` DESC ");
?>
<script>
	$(document).ready(function(){
		$(".notice-dismiss").click(function(){
			$(this).parent.hide();
		});
	});
</script>
<div id="wpbody" role="main">

<div id="wpbody-content" aria-label="Main content" tabindex="0">
		
<div class="wrap">

<h1>Paypal Payment Gateway Details</h1>
<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
	<p>
		<strong></strong>
	</p>
	<button type="button" class="notice-dismiss">
		<span class="screen-reader-text">Dismiss this notice.</span>
	</button>
</div>

<form method="post" action="" enctype="multipart/form-data">

	<table class="form-table">
		<tbody> <?php 
			 foreach($row as $row){ ?>
			<tr>
				<th scope="row"><label for="gateway">Gateway Environment</label></th>
				<td>
					<select name="gateway" value="<?php echo $row->pro_gateway;?>" class="regular-text" required/>
					
				  <option value="Sandbox/Testing"<?php if ($row->pro_gateway == "Sandbox/Testing") echo "selected='selected'";?>>Sandbox/Testing</option>
				  <option value="Live"<?php if ($row-> pro_gateway == "Live") echo "selected='selected'";?>>Live</option>
				</select>

				</td>
			</tr>
			
			<tr>
				<th scope="row"><label for="payemail">Email</label></th>
				<td>
					<input type="email" name="payemail" value="<?php echo $row->pro_payemail;?>" class="regular-text" required/>
				</td>
			</tr>
			<?php 
			 }?>
		</tbody>
	</table>
	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
</form>

</div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>

