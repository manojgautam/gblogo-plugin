<?php
/*
Plugin Name: Design Pro
Plugin URI: 
Description: Plugin to add and edit the transportation types from admin.
Author: Admin
Version: 1.0.1
Author URI:
*/

$siteurl = get_option('siteurl');
define('DP_FOLDER', dirname(plugin_basename(__FILE__)));
define('DP_URL', $siteurl.'/wp-content/plugins/' . DP_FOLDER);
define('DP_FILE_PATH', dirname(__FILE__));
define('DP_DIR_NAME', basename(DP_FILE_PATH));
// this is the table prefix
global $wpdb;
$wp_table_prefix=$wpdb->prefix;
define('DP_TABLE_PREFIX', $wp_table_prefix);

register_activation_hook(__FILE__,'DP_install');
register_deactivation_hook(__FILE__ , 'DP_uninstall' );

function DP_install()
{

$post_id = wp_insert_post($my_page);
	global $wpdb;
    
	/*	Create Orderfrom_pro table	*/
	$table = DP_TABLE_PREFIX."Orderfrom_pro";    
    $structure = "CREATE TABLE $table (
        logo_id INT(30) NOT NULL AUTO_INCREMENT,
        name VARCHAR(200) NOT NULL,
		price VARCHAR(11),
		tax VARCHAR(11),
		description TEXT,
		publish VARCHAR(11),
		categoryname VARCHAR(200),
		showdescription VARCHAR(250),
	    UNIQUE KEY logo_id (logo_id)
    );";
    $wpdb->query($structure);		// Execute query    
	wp_reset_query();	// Reset wordpress query
	
	$table = DP_TABLE_PREFIX."orderfrom_procategory";    
    $structure = "CREATE TABLE $table (
        cat_id INT(30) NOT NULL AUTO_INCREMENT,
        name VARCHAR(200) NOT NULL,
		short_code VARCHAR(50),
	    UNIQUE KEY cat_id (cat_id)
    );";
    $wpdb->query($structure);		// Execute query    
	wp_reset_query();	// Reset wordpress query
   
   	/*	Create benifical_logo_option table	*/
	$table = DP_TABLE_PREFIX."benifical_pro_option";    
    $structure = "CREATE TABLE $table (
        id INT(30) NOT NULL AUTO_INCREMENT,
        option_title VARCHAR(200) NOT NULL,
		option_price VARCHAR(11),
                option_tax VARCHAR(11),
		option_desc TEXT,
		option_cat VARCHAR(255),
		isall VARCHAR(255),
		showdescription VARCHAR(250),
	    UNIQUE KEY id (id)
    );";
    $wpdb->query($structure); // Execute query
wp_reset_query();	// Reset wordpress query

/*	Create payemetn_pro table	*/
	$table = DP_TABLE_PREFIX."payemetn_pro";    
    $structure = "CREATE TABLE $table (
        id INT(30) NOT NULL AUTO_INCREMENT,
        pro_gateway VARCHAR(50) NOT NULL,
		pro_payemail VARCHAR(50),
	    UNIQUE KEY id (id)
    );";
    $wpdb->query($structure); // Execute query
   wp_reset_query();	// Reset wordpress query
	$query = $wpdb->insert( $table, array(
		'pro_gateway' => 'Sandbox/Testing',
		'pro_payemail' =>'demo@gmail.com',
		));
		
		$table = DP_TABLE_PREFIX."step2_cat";    
    $structure = "CREATE TABLE $table (
        id INT(30) NOT NULL AUTO_INCREMENT,
        cat_name VARCHAR(50) NOT NULL,
	    UNIQUE KEY id (id)
    );";
	$wpdb->query($structure); // Execute query
   wp_reset_query();
   
   
   $table = DP_TABLE_PREFIX."wp_design_orderpro";    
    $structure = "CREATE TABLE $table (
        order_id int(20) NOT NULL AUTO_INCREMENT,
        cat_name VARCHAR(50) NOT NULL,
		t_id varchar(50),
		total_amount varchar(50),
		prouct_cat varchar(50),
		prouct_name varchar(255),
		optionproduct text,
		your_company varchar(50),
		overview varchar(255),
		target 	varchar(255),
		file1 varchar(255),
		file2 varchar(255),
		color_idea varchar(255),
		logofeel text,
		service text,
		logosidea varchar(255),
		rquirements varchar(255),
		yourname varchar(250),
		yournumber varchar(255),
		yourmail varchar(255),
		preferedcontact varchar(255),
		findus varchar(255),
		order_date varchar(250),
	    UNIQUE KEY id (order_id)
    );";
	$wpdb->query($structure); // Execute query
   wp_reset_query();
}
function DP_uninstall()
{
	global $wpdb;
    
	/*	Delete orderform table	*/
	$table = DP_TABLE_PREFIX."Orderfrom_pro";
    $structure = "drop table if exists $table";
    $wpdb->query($structure);  // Execute query
		
	wp_reset_query();	// Reset wordpress query

	/*	Delete	Orderfrom_logo table */
	$table = DP_TABLE_PREFIX."Orderfrom_pro";
    $structure = "drop table if exists $table";
    $wpdb->query($structure);  	// Execute Query
	
}

add_action('admin_menu','dp_menu');	// Admin menu hook

/*	Function is used to add a new menu in plugin 	*/
function dp_menu() 	
{ 
	add_menu_page(
		"Design Pro",
		"Design Pro",
		8,
		__FILE__,
		"dp_menu_list",
		DP_URL."/images/web.png"
	); 	// Create a main menu
		add_submenu_page(__FILE__,'Step1 Options','Step1 Options','8','add-prologo','dp_add_logo'); 	// Add submenus to the main menu
	add_submenu_page(__FILE__,'Step2 Category','Step2 Category','8','dp_step_category','dp_step_category');
	add_submenu_page(__FILE__,'Step2 Options','Step2 Options','8','dp_benifit_options','dp_benifit_list');	// Add submenus to the main menu
	add_submenu_page(__FILE__,'Payment Setting','Payment Setting','8','payment_setting','dp_payment_setting'); 	// Add submenus to the main menu
	add_submenu_page(__FILE__,'View Order','View Order','8','view_order','dp_view_order'); 	// Add submenus to the main menu
	add_submenu_page(__FILE__,'Order Detail','Order Detail','8','order_detail','dp_order_detail'); 	// Add submenus to the main menu
}

/*	Menu to list the logos main plugin page 	*/
function dp_menu_list()
{
	include'step1cat.php';	// Include the logo list page
}

/*	Function is used to add new logo 	*/
function dp_add_logo()
{
	include'step1.php';	// Include a add new logo form page
}
function dp_step_category()
{
	include 'step2cat.php';	// Include a add new logo form page
}
function dp_benifit_list()
{
	include'step2.php';
}
function dp_payment_setting()
{
	include'payment.php';
}
function dp_view_order()
{
	include'vieworder.php';
}
function dp_order_detail()
{
	include'orderdetail.php';
}
function my_shortcode_callback($atts){
 //global $codeid; 
 $codeid=$atts['id'];
 include 'design-pro-front-view.php';
}
add_shortcode(pro_logo_shortcode, 'my_shortcode_callback');
?>