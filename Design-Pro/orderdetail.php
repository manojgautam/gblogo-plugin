<h1>Order Detail</h1>
<?php
global $wpdb;
 $id=$_GET['id'];
 if($id==''){
 echo "not found";
 }
 else{
 
$table_name = $wpdb->prefix . "design_orderpro";
$rows = $wpdb->get_results("select * from $table_name where order_id= $id");
foreach($rows as $row) {?>
 
<table width="850px" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
	<tbody><tr>
    	<td valign="top" align="center">
            <!-- ========  design body starts here  ======== -->
            <table width="850" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
                <tbody><tr>
                    <td valign="top" align="center"><!-- ========  header  ======== -->
                        <!-- ========  header end  ======== -->
                        <!-- ========  design body starts here  ======== -->
                        <br>
                      <table width="800" cellspacing="3" cellpadding="3" border="0" style=" border:1px solid #DEE5C3;color:#7E7E7E;font-size:12px">
                        <tbody>
                          
                          
                          <tr>
                            <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Transaction Detail</div></td>
                          </tr>
                          
                          <tr bgcolor="#f6f7f1">
                            <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Payment Status:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php $status=$row->t_id; 
							  if($status!=''){
							  echo "Paid";
							  }?></td>
                          </tr>
                          
                          <tr bgcolor="#f6f7f1">
                            <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Payment Transaction ID:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->t_id; ?></td>
                          </tr>
                          
                          <tr bgcolor="#f6f7f1">
                            <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Order  No:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->order_id; ?></td>
                          </tr>
						  <tr bgcolor="#f6f7f1">
                            <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Order  Date:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->order_date; ?></td>
                          </tr>
                        </tbody></table>
  
      
              <br>  
                      
                      <!-- ========  columns group START here  ======== -->  
                 <table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0px auto; font-size:0;">
                        <tbody><tr>
                          <td valign="top" align="center">
                            <img width="800" border="0" align="bottom" height="15" style="width:700px; height:15px; padding:0px; margin:0px;" alt=" " src="<?php echo DP_URL; ?>/email/spacer.gif">  
                            <table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0px auto; font-size:0;">
                              <tbody><tr>
                                <td width="400" valign="top" align="left">
                                  <!-- ========  column 1  ======== --> 
                                  <table width="390" cellspacing="0" cellpadding="0" border="0" align="left" style="font-size:0;">
                                    <tbody><tr>
                                      <td height="42">
                                        
                                        <img width="390" border="0" height="42" alt="Open In Browser" src="<?php echo DP_URL; ?>/email/column_header.gif">
                                        
                                      </td>
                                    </tr>
                                    <tr>
                                      <td bgcolor="#cccccc" align="center">
                                        <table width="388" cellspacing="0" cellpadding="5" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
                                          <tbody><tr>
                                            <td valign="top" align="left">
                                              <table width="365" cellspacing="0" cellpadding="0" border="0" align="center">
                                                <tbody><tr>
                                                  <td valign="top" align="left">
                                                    <p>
                                                      <span style="font-weight:bold; font-size:20px; font-family:arial, verdana, tahoma; color:#95AD41;">Logo &amp; Stationery Options.</span>
                                                    </p>
                                                  </td>
                                                </tr>
                                              </tbody></table>
                                                                <img width="200" height="13" style="height:10px;" src="<?php echo DP_URL; ?>/email/spacer10.gif" alt="">
                                            </td>
                                          </tr>
                                        </tbody></table>
                                                      <table width="388" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
                                                        <tbody><tr>
                                                          <td valign="top" align="left"><table width="365" cellspacing="0" cellpadding="0" border="0" align="center" style="font-size:0;">
                                                            <tbody><tr>
                                                              <td valign="top" align="left">                                                               <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:14px;">
                          <tbody>
                          <tr>
                            <td width="66%" valign="top" align="left" height="39"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold; margin:10px 0;"><?php echo $row->prouct_cat; ?></div></td>
                            <td valign="bottom" align="right" height="39" class="small_txt"></td>
                          </tr> 
                        
							<tr>
                            <td valign="middle" align="left" height="39" style="border-bottom: 1px solid #DEE5C3; padding: 1px 0 0 10px; color: #95AD41;"><?php echo $row->prouct_name; ?></td>
                            <td width="28%" valign="middle" align="right" height="39" style="border-bottom: 1px solid #DEE5C3; padding: 1px 0 0 10px; color: #95AD41;"><strong></strong></td>
                          </tr>
						</tbody></table>
                                                                
                                                                
                                                                
                                                                <img width="200" height="13" style="height:10px;" src="<?php echo DP_URL; ?>/email/spacer10.gif" alt="">                                                              </td>
                                                            </tr>
                                                            
                                                            </tbody></table></td>
                                                        </tr>
                                        </tbody></table>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td valign="top" bgcolor="#ffffff" height="28" style="font-size:0;">
                                        <img width="390" height="28" src="<?php echo DP_URL; ?>/email/column_footer.png" alt="">
                                      </td>
                                    </tr>
                                  </tbody></table>
                                   <!-- ========  column 1 end  ======== -->
                                  
                                </td>
                                    
                                  <td width="400" valign="top" align="right">
                                    
                                    <!-- ========  column 3  ======== --> 
                                    <table width="390" cellspacing="0" cellpadding="0" border="0" align="right" style="font-size:0;">
                                      <tbody><tr>
                                        <td height="42">
                                          
                                          <img width="390" border="0" height="42" alt="Open In Browser" src="<?php echo DP_URL; ?>/email/column_header.gif">
                                          
                                        </td>
                                      </tr>
                                      <tr>
                                        <td bgcolor="#cccccc" align="center">
                                          <table width="388" cellspacing="0" cellpadding="5" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
                                            <tbody><tr>
                                              <td valign="top" align="left">
                                                <table width="365" cellspacing="0" cellpadding="0" border="0" align="center">
                                                  <tbody><tr>
                                                    <td valign="top" align="left">
                                                      <p>
                                                        <span style="font-weight:bold; font-size:20px; font-family:arial, verdana, tahoma; color:#95AD41;">Website Options</span><br>
														<?php $str= $row->optionproduct;
														$arr= explode("|",$str);
														$count= count($arr);
														for ($i = 0; $i < $count; $i++) {
                                                          echo $arr[$i]."<br>";
                                                        }
														?>
                                                      </p>
                                                    </td>
                                                  </tr>
                                                </tbody></table>
                                                                <img width="370" height="13" style="height:10px;" src="<?php echo DP_URL; ?>/email/spacer10.gif" alt="">
                                              </td>
                                            </tr>
                                          </tbody></table>
                                                      <table width="388" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
                                                        <tbody><tr>
                                                          <td valign="top" align="left"><table width="365" cellspacing="0" cellpadding="0" border="0" align="center" style="font-size:0;">
                                                            <tbody><tr>
                                                              <td valign="top" align="left"><br>
                                                                <?php echo $row->optionproduct; ?>
                                                                
                                                                
                                                                <img width="370" height="13" style="height:10px;" src="<?php echo DP_URL; ?>/email/spacer10.gif" alt="">
                                                              </td>
                                                            </tr>
                                                            
                                                            </tbody></table></td>
                                                        </tr>
                                          </tbody></table>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td valign="top" bgcolor="#ffffff" height="28" style="font-size:0;">
                                          <img width="390" height="28" src="<?php echo DP_URL; ?>/email/column_footer.png" alt="">
                                        </td>
                                      </tr>
                                    </tbody></table>
                                    <!-- ========  column 3 end  ======== -->
                                    
                                </td>
                              </tr>
                            </tbody></table>
                    </td>
                </tr>
                      </tbody></table>
             <p>
               <!-- ========  columns Group END here  ======== -->
               <img width="214" border="0" align="bottom" height="5" style="width:214px; height:5px; padding:0px; margin:0px;" alt=" " src="<?php echo DP_URL; ?>/email/spacer.gif">
               
               <!-- ========  featured product START here  ======== -->
               </p>
            <table width="788" cellspacing="0" cellpadding="15" border="0" bgcolor="#96AE3F" align="center" style="font-size:0;">
  <tbody><tr>
    <td width="549">
      <span style="color:#ffffff; font-size:24px; font-weight:bold; line-height:20px; font-family:arial, verdana, tahoma;">
        Total Amount </span>                                    </td>
  <td width="179" valign="middle" align="center"><span style="font-size: 24px;color: #FFFFFF;">£<?php echo $row->total_amount; ?> including VAT</span></span></td>
              </tr>
              </tbody></table><img width="214" border="0" align="bottom" height="5" style="width:214px; height:10px; padding:0px; margin:0px;" alt=" " src="<?php echo DP_URL; ?>/email/spacer.gif">
                      
                      
  <table width="800" cellspacing="0" cellpadding="0" border="0" align="center" style="font-size:0;">
    <tbody><tr>
      <td height="42">
        
        <img width="800" border="0" height="42" alt="Open In Browser" src="<?php echo DP_URL; ?>/email/column_header_full.gif">
        
        </td>
              </tr>
    <tr>
      <td bgcolor="#cccccc" align="center">
        <table width="798" cellspacing="0" cellpadding="5" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
          <tbody><tr>
            <td valign="top" align="left">
              <table width="765" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody><tr>
                  <td valign="top" align="left">
                    <p>
                      <span style="font-weight:bold; font-size:20px; font-family:arial, verdana, tahoma; color:#95AD41;">Design Brief</span>
                      </p>
                            </td>
                          </tr>
                </tbody></table>
                                                                <img width="200" height="13" style="height:10px;" src="<?php echo DP_URL; ?>/email/spacer10.gif" alt="">
              </td>
                    </tr>
          </tbody></table>
                                                      <table width="798" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="font-size:0;">
                                                        <tbody><tr>
                                                          <td valign="top" align="center">
                                                            
                                                            
                                                            <table width="380" cellspacing="3" cellpadding="3" border="0" style=" border:1px solid #DEE5C3;color:#7E7E7E;font-size:12px">
                                                              <tbody>
                                                                <tr bgcolor="#f6f7f1">
                                                                  <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Your Company Name:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->your_company; ?></td>
                            </tr>
                                                                <tr bgcolor="#f6f7f1">
                                                                  <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2"><strong>Brief Overview Of Your Business:
                                                                    </strong><br><br>
                                                                    <?php echo $row->overview; ?></td>
                              </tr>
                                                                <tr bgcolor="#f6f7f1">
                                                                  <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2"><strong>Target Audience:
                                                                    </strong><br><br><?php echo $row->target; ?>
                                                                    </td>
                              </tr>
                                                                
                                                                <tr>
                                                                  <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Your requirements?</div> <br><br><?php echo $row->rquirements; ?></td>
                            </tr>
                                                                <tr bgcolor="#f6f7f1">
                                                                  <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2">Open To Ideas<br>
                                                                    </td>
                              </tr>
                                                                
                                                                <tr>
                                                                  <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Do you have any specific colours you want to use?</div></td>
                            </tr>
                                                                <tr bgcolor="#f6f7f1">
                                                                  <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2"> 

																	<?php echo $row->logosidea; ?></td>
                              </tr>
                                                                
                                                                <tr>
                                                                  <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Overall feel of the logo design</div>
																  <br><br><?php echo $row->logofeel; ?></td>
                            </tr>
                                                                <tr bgcolor="#f6f7f1">
                                                                  <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2">Professional<br>
  <br>
                                                                    Other (Please Type Here)</td>
                              </tr>
                                                                
                                                                
                                                              </tbody></table>
                                                          
                                                        
                                                          
                                                        </td>
                                                           <td valign="top" align="center">
                                                             
                                                             <table width="380" cellspacing="3" cellpadding="3" border="0" style=" border:1px solid #DEE5C3;color:#7E7E7E;font-size:12px">
                                                               <tbody>
                                                                 
                                                                 
                                                                 <tr>
                                                                   <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Logos you like (Optional)</div><?php echo $row->file1; ?></td>
                            </tr>
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2">Please Type Here<br>
                                                                     
                                                                     
                                                                     </td>
                              </tr>
                                                                 
                                                                 <tr>
                                                                   <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Upload supporting file (Optional)</div></td>
                            </tr>
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2">
                                                                     <?php echo $row->file2; ?>
                                                                     </td>
                              </tr>
                                                                 
                                                                 
                                                                 <tr>
                                                                   <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Additional services you may require?</div></td>
                            </tr>
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td valign="middle" align="left" height="40" style="padding-left:10px;" colspan="2">
                                                                     <?php echo $row->service; ?>
                                                                     </td>
                              </tr>
                                                                 
                                                                 
                                                                 <tr>
                                                                   <td valign="bottom" align="left" height="56" colspan="2"><div style="color: #95AD41;font-family: Arial,Helvetica,sans-serif;font-size: 18px;font-weight: normal; margin: 0 0 10px 0;">Contact info</div></td>
                            </tr>
                                                                 
                                                                 
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Your Name:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->yourname; ?></td>
                            </tr>
                                                                 
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Your Email</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->yourmail; ?></td>
                            </tr>
                                                                 
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Your Phone No:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->yournumber; ?></td>
                            </tr>
                                                                 
                                                                 <tr bgcolor="#f6f7f1">
                                                                   <td width="50%" valign="middle" align="left" height="40" style="padding-left:10px;"><strong>Preferd Method of contact:</strong></td>
                              <td width="50%" valign="middle" align="left" style="padding-left:10px;"><?php echo $row->preferedcontact; ?></td>
                            </tr>
                                                               </tbody></table>
                                                           </td>
                                                        </tr>
                  </tbody></table>
                </td>
              </tr>
    <tr>
      <td valign="top" bgcolor="#ffffff" height="28" style="font-size:0;">
        <img width="800" height="28" src="<?php echo DP_URL; ?>/email/column_footer_full.png" alt="">
        </td>
              </tr>
    </tbody></table>
  
           <p>
             <!-- ========  featured product END here  ======== -->
             <!-- ========  Overview  ======== -->
             <img width="214" border="0" align="bottom" height="15" style="width:214px; height:15px; padding:0px; margin:0px;" alt=" " src="<?php echo DP_URL; ?>/email/spacer.gif">
             </p>
                    </td></tr>
        </tbody></table>
         <!-- ========  design body ends here  ======== -->       
 </td>
    </tr>
</tbody></table>
 
<?php } 
}
?>